/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.authentication.api;

import java.util.Optional;

import org.amdatu.authentication.api.exceptions.InvalidPasswordResetException;
import org.amdatu.authentication.api.exceptions.MemberNotFoundException;
import org.amdatu.mongo.crud.BaseMongoCrudService;
import org.mongojack.DBQuery;
import org.mongojack.JacksonDBCollection;
import org.mongojack.WriteResult;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Sets;
import com.mongodb.BasicDBObject;

public abstract class BaseMongoAuthService<T extends Member> extends BaseMongoCrudService<T> implements AuthService<T>{

	private volatile LoadingCache<String, Optional<T>> m_memberByEmailCache = CacheBuilder.newBuilder()
		       .maximumSize(1000)
		       .build(
		           new CacheLoader<String, Optional<T>>() {
		             public Optional<T> load(String key)  {
		            	 T findOne = m_collection.findOne(DBQuery.is("email", key));
		            	 return Optional.ofNullable(findOne);
		             }
		           }); 
	
	private volatile JacksonDBCollection<PasswordResetEvent, String> m_passwordEvents;
	
	public BaseMongoAuthService(String collectionName, Class<T> clazz) {
		super(collectionName, clazz);
	}
	
	public void start() {
		super.start();
		
		m_passwordEvents = JacksonDBCollection.wrap(getMongoDbService().getDB().getCollection("password-reset-events"), PasswordResetEvent.class, String.class);
		
		m_collection.getDbCollection().ensureIndex(new BasicDBObject("authenticationIds", 1), new BasicDBObject("name", "members_authid"));
		m_collection.getDbCollection().ensureIndex(new BasicDBObject("email", 1), new BasicDBObject("name", "members_email"));
		m_collection.getDbCollection().ensureIndex(new BasicDBObject("affiliateIds", 1), new BasicDBObject("name", "members_affiliateIds"));
	}
	
	@Override
	protected void postSave(T obj, String id) {
		super.postSave(obj, id);
		m_memberByEmailCache.invalidate(obj.getEmail());
	}

	@Override
	public Optional<T> findMemberByAuthId(String authId) {
		return Optional.ofNullable(m_collection.findOne(new BasicDBObject("authenticationIds", authId)));
	}

	@Override
	public Optional<T> findMemberByEmail(String email) {
		return m_memberByEmailCache.getUnchecked(email);
	}
	
	@Override
	public void preparePasswordReset(String email) throws MemberNotFoundException {
		Optional<T> member = findMemberByEmail(email);
		if(!member.isPresent()) {
			throw new MemberNotFoundException();
		}
		
		PasswordResetEvent passwordResetEvent = new PasswordResetEvent(email, System.currentTimeMillis()); 
		WriteResult<PasswordResetEvent, String> id = m_passwordEvents.save(passwordResetEvent);
		
		getEmailService().sendPasswordResetEmail(member.get(), id.getSavedId());
	}

	@Override
	public T resetPassword(String resetToken, String newPassword) {
		PasswordResetEvent resetEvent = m_passwordEvents.findOneById(resetToken);
		long minTime = System.currentTimeMillis() - 7200000;
		
		if(resetEvent != null && resetEvent.getTimestamp() > minTime) {
			m_passwordEvents.removeById(resetToken);
			T member = findMemberByEmail(resetEvent.getEmail()).get();
			String authId = generateAuthId(newPassword);
			member.setAuthenticationIds(Sets.newHashSet(authId));
			save(member);
			
			return member;
		} else {
			throw new InvalidPasswordResetException();
		}
	}
	
	@Override
	public String generateAuthId(String password) {
		return PasswordHash.createHash(password);
	}

	protected abstract PasswordResetEmailService getEmailService();
}
