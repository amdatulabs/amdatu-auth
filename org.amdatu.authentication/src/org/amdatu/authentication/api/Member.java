/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.authentication.api;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.mongojack.ObjectId;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Member {
	@ObjectId
	protected String _id;
	
	protected String m_firstname;
	protected String m_lastname;
	
	protected Set<String> m_authenticationIds = new HashSet<>();
	protected Set<String> m_roles = new HashSet<>();
	
	@NotNull @Email
	protected String m_email;
	
	protected String m_language = "en";
	

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getFirstname() {
		return m_firstname;
	}

	public void setFirstname(String firstname) {
		m_firstname = firstname;
	}

	public String getLastname() {
		return m_lastname;
	}

	public void setLastname(String lastname) {
		m_lastname = lastname;
	}

	public Set<String> getAuthenticationIds() {
		return m_authenticationIds;
	}

	public void setAuthenticationIds(Set<String> authenticationIds) {
		m_authenticationIds = authenticationIds;
	}

	public Set<String> getRoles() {
		return m_roles;
	}

	public void setRoles(Set<String> roles) {
		m_roles = roles;
	}

	public String getEmail() {
		return m_email;
	}

	public void setEmail(String email) {
		m_email = email;
	}

	public String getLanguage() {
		return m_language;
	}

	public void setLanguage(String language) {
		m_language = language;
	}
}
