/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.authentication.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.amdatu.authentication.api.AuthService;
import org.amdatu.authentication.api.Member;
import org.amdatu.authentication.api.PasswordHash;
import org.amdatu.authentication.api.exceptions.InvalidPasswordResetException;
import org.amdatu.authentication.api.exceptions.MemberNotFoundException;
import org.amdatu.authentication.rest.data.Login;
import org.amdatu.authentication.rest.data.NewMember;
import org.amdatu.authentication.rest.data.PasswordResetSpec;
import org.amdatu.authentication.rest.exceptions.NotAuthorizedException;
import org.amdatu.authentication.web.WebAuthService;
import org.amdatu.security.tokenprovider.TokenProvider;
import org.amdatu.security.tokenprovider.TokenProviderException;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

import com.google.common.base.Strings;

@Component(provides=Object.class)
@Path("auth")
public class AuthenticationResource {
	
	
	@ServiceDependency
	private volatile AuthService<Member> m_memberService;

	@ServiceDependency
	private volatile WebAuthService m_webAuthService;
	
	@ServiceDependency
	private volatile TokenProvider m_tokenProvider;
	
	@ServiceDependency
	private volatile EventAdmin m_eventAdmin;
	
	@GET
	@Path("me")
	@Produces(MediaType.APPLICATION_JSON)
	public Member me(@Context HttpServletRequest request) {
		return m_webAuthService.getMemberFromRequest(request);
	}
	
	@GET
	@Path("{memberId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Member getMember(@Context HttpServletRequest request, @PathParam("memberId") String memberId)  {

		Member memberFromRequest = m_webAuthService.getMemberFromRequest(request);
		if(memberFromRequest.get_id().equals(memberId) || m_webAuthService.isAdmin(memberFromRequest)) {
			return m_memberService.findById(memberId).orElseThrow(() -> new MemberNotFoundException());	
		} else {
			throw new NotAuthorizedException();
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Member> members(@Context HttpServletRequest request) {
		Member member = m_webAuthService.getMemberFromRequest(request);
		if(!m_webAuthService.isAdmin(member)) {
			throw new NotAuthorizedException();
		}
		
		return m_memberService.list();
	}
	
	@Path("login")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response login(Login login, @Context HttpServletRequest request) {
		
		if(Strings.isNullOrEmpty(login.getEmail()) || Strings.isNullOrEmpty(login.getPassword())) {
			return Response.status(Status.BAD_REQUEST).entity("Missing either email or password").build();
		}
		
		Optional<Member> member = m_memberService.findMemberByEmail(login.getEmail());
		if (!member.isPresent()) {
			return Response.status(Status.FORBIDDEN).build();
		}
		boolean isValid = member.get().getAuthenticationIds().stream().anyMatch(correctHash -> {
			return PasswordHash.validatePassword(login.getPassword(), correctHash);
		});
		if (!isValid) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		return createLoginResponse(member.get(), request);
	}
	
	private Response createLoginResponse(Member member, HttpServletRequest request) {
		SortedMap<String, String> memberData = new TreeMap<>();
		memberData.put(TokenProvider.USERNAME, member.getEmail());
		memberData.put("memberId", member.get_id());
		
		try {
			String token = m_tokenProvider.generateToken(memberData);
			return Response.ok(member).header("Set-Cookie", createCookie(request, token, Integer.MAX_VALUE)).build();				
		} catch (TokenProviderException e) {
			throw new RuntimeException(e);
		}
	}
	
	private String createCookie(HttpServletRequest request, String identifier, int maxAge) {
        StringBuilder cookieStr = new StringBuilder();
        // the name/value of the cookie...
        cookieStr.append(TokenProvider.TOKEN_COOKIE_NAME).append("=").append(identifier);
        // determine the path (and sub-paths) from which it should be served...
        cookieStr.append(";Path=/");
        cookieStr.append(";max-age=").append(maxAge);
        if (request.isSecure()) {
            // When running on HTTPS, force the browser to only sent the cookie back via HTTPS...
            cookieStr.append(";Secure");
        }
        // We only use this cookie at the server-side, so do not let JavaScript access this cookie...
        cookieStr.append(";HttpOnly");
        return cookieStr.toString();
    }
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{memberId}")
	public Response update(@PathParam("memberId") String memberId, NewMember memberToSave, @Context HttpServletRequest request) {
		if(!memberId.equals(memberToSave.get_id())) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		Member memberFromRequest = m_webAuthService.getMemberFromRequest(request);
		
		if(memberFromRequest.get_id().equals(memberToSave.get_id()) || m_webAuthService.isAdmin(memberFromRequest)) {
			if (memberToSave.getPassword() != null) {
				memberToSave.getAuthenticationIds().clear(); // remove old authId first
				memberToSave.getAuthenticationIds().add(m_memberService.generateAuthId(memberToSave.getPassword()));
				
				Map<String, Object> properties = new HashMap<>();
				properties.put("user", memberToSave);
				properties.put("type", "passwordchange");
				properties.put("password", memberToSave.getPassword());
				Event event = new Event("org/amdatu/authentication", properties);
				m_eventAdmin.postEvent(event);
			}
			
			String password = memberToSave.getPassword();
			memberToSave.setPassword(null);
			m_memberService.save(memberToSave);
			
			Map<String, Object> properties = new HashMap<>();
			properties.put("user", memberToSave);
			properties.put("type", "update");
			properties.put("password", password);
			
			Event event = new Event("org/amdatu/authentication", properties);
			m_eventAdmin.postEvent(event);
			
			return Response.ok().build();
			
		} else {
			return Response.status(Status.FORBIDDEN).build();
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Member newMember(NewMember memberToSave, @Context HttpServletRequest request) {
		Member memberFromRequest = m_webAuthService.getMemberFromRequest(request);
		
		if(m_webAuthService.isAdmin(memberFromRequest)) {
						
			memberToSave.getAuthenticationIds().add(m_memberService.generateAuthId(memberToSave.getPassword()));
			
			String password = memberToSave.getPassword();
			memberToSave.setPassword(null);

			String savedId = m_memberService.save(memberToSave);
			memberToSave.set_id(savedId);
			
			Map<String, Object> properties = new HashMap<>();
			properties.put("user", memberToSave);
			properties.put("type", "new");
			properties.put("password", password);
			
			Event event = new Event("org/amdatu/authentication", properties);
			m_eventAdmin.postEvent(event);
			
			
			return memberToSave;
			
		} else {
			throw new NotAuthorizedException();
		}
	}
	
	@GET
	@Path("emailavailable")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean isEmailAvailable(@QueryParam("email") String email) {
		Optional<Member> existing = m_memberService.findMemberByEmail(email);
		return !existing.isPresent();
	}
	
	@Path("logout")
	@POST
	public void logout(@Context HttpServletRequest request) {
		String tokenFromRequest = m_tokenProvider.getTokenFromRequest(request);
		m_tokenProvider.invalidateToken(tokenFromRequest);
	}
	
	@Path("passwordreset")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response passwordReset(PasswordResetSpec passwordResetSpec) {
		
		if(passwordResetSpec.token == null) {
			m_memberService.preparePasswordReset(passwordResetSpec.email);
		} else {
			try {				
				Member member = m_memberService.resetPassword(passwordResetSpec.token, passwordResetSpec.password);
				
				Map<String, Object> properties = new HashMap<>();
				properties.put("user", member);
				properties.put("type", "passwordchange");
				properties.put("password", passwordResetSpec.password);
				Event event = new Event("org/amdatu/authentication", properties);
				m_eventAdmin.postEvent(event);
			} catch(InvalidPasswordResetException ex) {
				return Response.status(Status.FORBIDDEN).build();
			}
		}
		
		return Response.ok().build();
	}
	
	@Path("admin")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public boolean isAdmin(@Context HttpServletRequest request) {
		Member member = m_webAuthService.getMemberFromRequest(request);
		return m_webAuthService.isAdmin(member);
	}
	
	@Path("{memberId}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public void delete(@Context HttpServletRequest request, @PathParam("memberId") String memberToDelete) {
		Member member = m_webAuthService.getMemberFromRequest(request);
		if(m_webAuthService.isAdmin(member)) {
			
			Optional<Member> memberBeforeDelete = m_memberService.findById(memberToDelete);
			
			m_memberService.delete(memberToDelete);
			
			if(memberBeforeDelete.isPresent()) {
				Map<String, Object> properties = new HashMap<>();
				properties.put("user", memberBeforeDelete.get());
				properties.put("type", "delete");
				Event event = new Event("org/amdatu/authentication", properties);
				m_eventAdmin.postEvent(event);
			}
		} else {
			throw new NotAuthorizedException();			
		}
	}
}
