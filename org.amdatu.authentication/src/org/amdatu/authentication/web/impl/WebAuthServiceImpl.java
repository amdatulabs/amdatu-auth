/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.authentication.web.impl;

import java.util.Optional;
import java.util.SortedMap;

import javax.servlet.http.HttpServletRequest;

import org.amdatu.authentication.api.AuthService;
import org.amdatu.authentication.api.Member;
import org.amdatu.authentication.api.exceptions.MemberNotFoundException;
import org.amdatu.authentication.web.WebAuthService;
import org.amdatu.security.tokenprovider.InvalidTokenException;
import org.amdatu.security.tokenprovider.TokenProvider;
import org.amdatu.security.tokenprovider.TokenProviderException;
import org.amdatu.security.tokenprovider.TokenUtil;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class WebAuthServiceImpl implements WebAuthService{

	@ServiceDependency
	private volatile AuthService<Member> m_memberService;
	
	@ServiceDependency
	private volatile TokenProvider m_tokenProvider;
	
	@Override
	public Member getMemberFromRequest(HttpServletRequest request) throws MemberNotFoundException, RuntimeException {
		String memberId = getMemberIdFromRequest(request);
		Optional<Member> member = m_memberService.findById(memberId);
		
		if(!member.isPresent()) {
			throw new MemberNotFoundException(memberId);
		}
		
		return member.get();
	}
	
	@Override
	public String getMemberIdFromRequest(HttpServletRequest request) throws RuntimeException {
		try {
			String tokenFromRequest = TokenUtil.getTokenFromRequest(request);
			SortedMap<String, String> token = m_tokenProvider.verifyToken(tokenFromRequest);
			String memberId = token.get("memberId");
			return memberId;
		} catch (TokenProviderException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public boolean isAdmin(Member member) {
		return member != null && member.getRoles().contains("ADMIN");
	}
}
