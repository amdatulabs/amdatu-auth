/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.authentication.web;

import javax.servlet.http.HttpServletRequest;

import org.amdatu.authentication.api.Member;
import org.amdatu.authentication.api.exceptions.MemberNotFoundException;

public interface WebAuthService {

	/**
	 * @param request
	 * @return the full member entry associated with the token in the given request
	 * @throws MemberNotFoundException if a member cannot be found for the associated memberId
	 * @throws RuntimeException if missing or invalid token in the request
	 */
	Member getMemberFromRequest(HttpServletRequest request) throws MemberNotFoundException, RuntimeException;

	/**
	 * @param request
	 * @return the memberId
	 * @throws RuntimeException if missing or invalid token in the request 
	 */
	String getMemberIdFromRequest(HttpServletRequest request) throws RuntimeException;

	boolean isAdmin(Member member);

}
