/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.authentication.mongo;

import org.amdatu.authentication.api.AuthService;
import org.amdatu.authentication.api.BaseMongoAuthService;
import org.amdatu.authentication.api.Member;
import org.amdatu.authentication.api.PasswordResetEmailService;
import org.amdatu.mongo.MongoDBService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.osgi.service.event.EventAdmin;

@Component
public class DefaultAuthService extends BaseMongoAuthService<Member> implements AuthService<Member>{

	@ServiceDependency
	private volatile MongoDBService m_mongoDbService;
	
	@ServiceDependency
	private volatile PasswordResetEmailService m_emailService;
	
	@ServiceDependency
	private volatile EventAdmin m_eventAdmin;

	public DefaultAuthService() {
		super("members", Member.class);
	}
	
	@Start
	public void start() {
		super.start();
	}
	
	@Override
	protected MongoDBService getMongoDbService() {
		return m_mongoDbService;
	}

	@Override
	protected PasswordResetEmailService getEmailService() {
		return m_emailService;
	}
}
