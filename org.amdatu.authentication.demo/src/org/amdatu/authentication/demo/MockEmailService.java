/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.authentication.demo;

import org.amdatu.authentication.api.Member;
import org.amdatu.authentication.api.PasswordResetEmailService;
import org.apache.felix.dm.annotation.api.Component;

@Component
public class MockEmailService implements PasswordResetEmailService{

	@Override
	public void sendPasswordResetEmail(Member member, String resetToken) {
		System.out.println("Sending mail to " + member.getEmail() + " with token " + resetToken);
	}

}
