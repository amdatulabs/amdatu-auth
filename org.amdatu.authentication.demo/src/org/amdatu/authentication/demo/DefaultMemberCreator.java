/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.authentication.demo;

import java.util.Optional;

import org.amdatu.authentication.api.AuthService;
import org.amdatu.authentication.api.Member;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;

@Component
public class DefaultMemberCreator {

	@ServiceDependency
	private volatile AuthService<Member> m_authService;
	
	@Start
	public void start() {
		String generateAuthId = m_authService.generateAuthId("test");
		Optional<Member> existingMember = m_authService.findMemberByAuthId(generateAuthId);
		if(!existingMember.isPresent()) {
			Member member = new Member();
			member.setFirstname("admin");
			member.setEmail("admin@amdatu.org");
			member.getRoles().add("ADMIN");
			member.getAuthenticationIds().add(generateAuthId);
			
			m_authService.save(member);
		}
	}
}
