/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.authentication.rest.itest;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.amdatu.testing.mongo.OSGiMongoTestConfigurator.configureMongoDb;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.amdatu.authentication.api.AuthService;
import org.amdatu.authentication.api.Member;
import org.amdatu.authentication.itest.MockEmailService;
import org.amdatu.authentication.rest.data.Login;
import org.amdatu.authentication.rest.data.PasswordResetSpec;
import org.amdatu.mongo.MongoDBService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpClient.BoundRequestBuilder;
import com.ning.http.client.Response;
import com.ning.http.client.cookie.Cookie;

public class RestAuthTest {
	
	private volatile AuthService<Member> m_authService;
	private volatile MockEmailService m_emailService;
	
	private final ObjectMapper mapper = new ObjectMapper();
	private Member m_member;
	
	@Before
	public void setup() throws InterruptedException {
		configure(this).add(createServiceDependency().setService(AuthService.class).setRequired(true))
				.add(configureMongoDb())
				.add(createConfiguration("org.amdatu.security.tokenprovider").set("secretkey", "[randomkey]"))
				.add(createServiceDependency().setService(MockEmailService.class).setRequired(true))
				.add(createServiceDependency().setService(MongoDBService.class).setRequired(true))
				.apply();
		
		m_member = new Member();
		m_member.setEmail("test@amdatu.org");
		m_member.getAuthenticationIds().add(m_authService.generateAuthId("test"));
		
		String savedId = m_authService.save(m_member);
		
		m_member.set_id(savedId);
		TimeUnit.SECONDS.sleep(1);
		
		m_emailService.reset();
	}
	
	@Test
	public void me() throws InterruptedException, ExecutionException, IOException, TimeoutException {
		
		try(AsyncHttpClient asyncHttpClient = new AsyncHttpClient()) {
			BoundRequestBuilder get = asyncHttpClient.prepareGet("http://localhost:8080/auth/me");
			get.setCookies(login());
			
			Future<Response> f = get.execute();
			Response r = f.get(1, TimeUnit.SECONDS);
			ObjectMapper mapper = new ObjectMapper();
			
			Member member = mapper.readValue(r.getResponseBody(), Member.class);
			
			assertEquals("test@amdatu.org", member.getEmail());
		}
	}
	
	@Test
	public void update() throws Exception {
		
		try(AsyncHttpClient asyncHttpClient = new AsyncHttpClient()) {
			String url = "http://localhost:8080/auth/" + m_member.get_id();
			BoundRequestBuilder put = asyncHttpClient.preparePut(url);
			put.addHeader("Content-Type", "application/json");
			put.setCookies(login());
			
			m_member.setFirstname("Amdatu");
			m_member.setEmail("othertest@amdatu.org");
			put.setBody(mapper.writeValueAsBytes(m_member));
			
			Future<Response> f = put.execute();
			Response r = f.get(1, TimeUnit.SECONDS);
			assertEquals(200, r.getStatusCode());
			
			Member updatedMember = m_authService.findMemberByEmail("othertest@amdatu.org").get();
			assertEquals("Amdatu", updatedMember.getFirstname());
			assertEquals("othertest@amdatu.org", updatedMember.getEmail());
			assertTrue(m_member.getAuthenticationIds().equals(updatedMember.getAuthenticationIds()));
		}
	}
	
	@Test
	public void emailAvailable() throws Exception {
		try(AsyncHttpClient asyncHttpClient = new AsyncHttpClient()) {
			BoundRequestBuilder get = asyncHttpClient.prepareGet("http://localhost:8080/auth/emailavailable?email=available@amdatu.org");
			
			Future<Response> f = get.execute();
			Response r = f.get(1, TimeUnit.SECONDS);
			boolean available = Boolean.parseBoolean(r.getResponseBody());
			
			assertTrue(available);
		}
	}
	
	@Test
	public void emailNotAvailable() throws Exception {
		try(AsyncHttpClient asyncHttpClient = new AsyncHttpClient()) {
			BoundRequestBuilder get = asyncHttpClient.prepareGet("http://localhost:8080/auth/emailavailable?email=test@amdatu.org");
			
			Future<Response> f = get.execute();
			Response r = f.get(1, TimeUnit.SECONDS);
			boolean available = Boolean.parseBoolean(r.getResponseBody());
			
			assertFalse(available);
		}
	}
	
	@Test
	public void list() throws InterruptedException, ExecutionException, IOException, TimeoutException {
		m_member.getRoles().add("ADMIN");
		m_authService.save(m_member);
		
		
		try(AsyncHttpClient asyncHttpClient = new AsyncHttpClient()) {
			BoundRequestBuilder get = asyncHttpClient.prepareGet("http://localhost:8080/auth");
			get.setCookies(login());
			
			Future<Response> f = get.execute();
			Response r = f.get(1, TimeUnit.SECONDS);
			ObjectMapper mapper = new ObjectMapper();
			JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, Member.class);
			List<Member> members = mapper.readValue(r.getResponseBody(), type);
			
			assertEquals(1, members.size());
		}
	}
	
	@Test
	public void listNotAuthorized() throws InterruptedException, ExecutionException, IOException, TimeoutException {
		try(AsyncHttpClient asyncHttpClient = new AsyncHttpClient()) {
			BoundRequestBuilder get = asyncHttpClient.prepareGet("http://localhost:8080/auth");
			get.setCookies(login());
			
			Future<Response> f = get.execute();
			Response r = f.get(1, TimeUnit.SECONDS);
			
			assertEquals(403, r.getStatusCode());
		}
	}
	
	@Test
	public void getMemberByAdmin() throws Exception {
		Member otherMember = new Member();
		otherMember.setEmail("other@amdatu.org");
		String otherMemberId = m_authService.save(otherMember);
		
		m_member.getRoles().add("ADMIN");
		m_authService.save(m_member);
		
		
		try(AsyncHttpClient asyncHttpClient = new AsyncHttpClient()) {
			BoundRequestBuilder get = asyncHttpClient.prepareGet("http://localhost:8080/auth/" + otherMemberId);
			get.setCookies(login());
			
			Future<Response> f = get.execute();
			Response r = f.get(1, TimeUnit.SECONDS);
			ObjectMapper mapper = new ObjectMapper();
			Member member = mapper.readValue(r.getResponseBody(), Member.class);
			
			assertEquals(otherMember.getEmail(), member.getEmail());
		}
	}
	
	@Test
	public void getMemberBySelf() throws Exception {
		try(AsyncHttpClient asyncHttpClient = new AsyncHttpClient()) {
			BoundRequestBuilder get = asyncHttpClient.prepareGet("http://localhost:8080/auth/" + m_member.get_id());
			get.setCookies(login());
			
			Future<Response> f = get.execute();
			Response r = f.get(1, TimeUnit.SECONDS);
			ObjectMapper mapper = new ObjectMapper();
			Member member = mapper.readValue(r.getResponseBody(), Member.class);
			
			assertEquals(m_member.getEmail(), member.getEmail());
		}
	}
	
	@Test
	public void getMemberNotAuthorized() throws Exception {
		Member otherMember = new Member();
		otherMember.setEmail("other@amdatu.org");
		String otherMemberId = m_authService.save(otherMember);
		
		try(AsyncHttpClient asyncHttpClient = new AsyncHttpClient()) {
			BoundRequestBuilder get = asyncHttpClient.prepareGet("http://localhost:8080/auth/" + otherMemberId);
			get.setCookies(login());
			
			Future<Response> f = get.execute();
			Response r = f.get(1, TimeUnit.SECONDS);
			
			assertEquals(403, r.getStatusCode());
		}
	}
	
	@Test
	public void badLogin() throws Exception {
		try(AsyncHttpClient asyncHttpClient = new AsyncHttpClient()) {
			BoundRequestBuilder preparePost = asyncHttpClient.preparePost("http://localhost:8080/auth/login");
			preparePost.addHeader("Content-Type", "application/json");
			
			Login login = new Login();
			login.setEmail("test@amdatu.org");
			login.setPassword("wrongPassword");
			preparePost.setBody(mapper.writeValueAsBytes(login));
			Response response = preparePost.execute().get(1, TimeUnit.SECONDS);
			
			assertEquals(response.getStatusCode(), 403);
		}
	}
	
	@Test
	public void logout() throws Exception {
		try(AsyncHttpClient asyncHttpClient = new AsyncHttpClient()) {
			BoundRequestBuilder logoutPost = asyncHttpClient.preparePost("http://localhost:8080/auth/logout");
			List<Cookie> tokenCookie = login();
			logoutPost.setCookies(tokenCookie);
			
			Future<Response> f = logoutPost.execute();
			Response r = f.get(1, TimeUnit.SECONDS);
			
			assertEquals(204, r.getStatusCode());
			
			BoundRequestBuilder get = asyncHttpClient.prepareGet("http://localhost:8080/auth/me");
			get.setCookies(tokenCookie);
			
			Future<Response> request2 = get.execute();
			Response r2 = request2.get(1, TimeUnit.SECONDS);
			assertEquals(500, r2.getStatusCode());
		}
	}
	
	@Test
	public void passwordResetRequest() throws Exception {
		try(AsyncHttpClient asyncHttpClient = new AsyncHttpClient()) {
			BoundRequestBuilder post = asyncHttpClient.preparePost("http://localhost:8080/auth/passwordreset");
			PasswordResetSpec passwordReset = new PasswordResetSpec();
			passwordReset.email = "test@amdatu.org";
			post.setBody(mapper.writeValueAsBytes(passwordReset));
			post.addHeader("Content-Type", "application/json");
			
			Future<Response> f = post.execute();
			Response r = f.get(1, TimeUnit.SECONDS);
			
			assertEquals(200, r.getStatusCode());
			assertEquals(1, m_emailService.getEventLog().size());
			String token = m_emailService.getEventLog().get(0).getToken();
			
			BoundRequestBuilder post2 = asyncHttpClient.preparePost("http://localhost:8080/auth/passwordreset");
			post2.addHeader("Content-Type", "application/json");
			passwordReset.token = token;
			passwordReset.password = "newpassword";
			post2.setBody(mapper.writeValueAsBytes(passwordReset));
			
			Future<Response> request2 = post2.execute();
			Response r2 = request2.get(1, TimeUnit.SECONDS);
			assertEquals(200, r2.getStatusCode());
			
			BoundRequestBuilder preparePost = asyncHttpClient.preparePost("http://localhost:8080/auth/login");
			preparePost.addHeader("Content-Type", "application/json");
			
			Login login = new Login();
			login.setEmail("test@amdatu.org");
			login.setPassword("newpassword");
			preparePost.setBody(mapper.writeValueAsBytes(login));
			Response loginResponse = preparePost.execute().get(1, TimeUnit.SECONDS);
			
			assertEquals(200, loginResponse.getStatusCode());
		}
	}
	
	private List<Cookie> login() {
		try(AsyncHttpClient asyncHttpClient = new AsyncHttpClient()) {
			
			BoundRequestBuilder preparePost = asyncHttpClient.preparePost("http://localhost:8080/auth/login");
			preparePost.addHeader("Content-Type", "application/json");
			
			Login login = new Login();
			login.setEmail("test@amdatu.org");
			login.setPassword("test");
			preparePost.setBody(mapper.writeValueAsBytes(login));
			Response response = preparePost.execute().get(1, TimeUnit.SECONDS);
			
			return response.getCookies();
		} catch (JsonProcessingException | InterruptedException | ExecutionException | TimeoutException e) {
			throw new RuntimeException(e);
		}
	}
	
	@After
	public void cleanup() {
		cleanUp(this);
	}
}
