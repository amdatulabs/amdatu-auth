package org.amdatu.authentication.itest;

import java.util.List;

import org.amdatu.authentication.itest.MockEmailServiceImpl.EmailEvent;

public interface MockEmailService {

	List<EmailEvent> getEventLog();

	void reset();

}
