/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.authentication.itest;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.amdatu.testing.mongo.OSGiMongoTestConfigurator.configureMongoDb;
import static org.junit.Assert.*;

import java.util.List;
import java.util.Optional;

import org.amdatu.authentication.itest.example.CustomAuthService;
import org.amdatu.authentication.itest.example.SportyMember;
import org.amdatu.mongo.MongoDBService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This tests shows usage of a custom Auth service implementation.
 */
public class MemberServiceTest {

	private volatile CustomAuthService serviceToTest;

	@Before
	public void setup() {
		configure(this).add(createServiceDependency().setService(CustomAuthService.class).setRequired(true))
				.add(configureMongoDb())
				.add(createServiceDependency().setService(MongoDBService.class).setRequired(true)).apply();
	}
	
	@Test
	public void saveAndRetrieveMember() {
		
		SportyMember member = new SportyMember();
		member.setEmail("test@amdatu.org");
		member.setMaxDeadlift(150);
		
		String id = serviceToTest.save(member);
		
		Optional<SportyMember> found = serviceToTest.findById(id);
		assertEquals(150, found.get().getMaxDeadlift(), 0.1);
	}		
	
	@Test
	public void authenticate() {
		SportyMember member = new SportyMember();
		member.setEmail("test@amdatu.org");
		String generateAuthId = serviceToTest.generateAuthId("test");
		
		member.getAuthenticationIds().add(generateAuthId);
		serviceToTest.save(member);
		
		Optional<SportyMember> authenticatedMember = serviceToTest.findMemberByAuthId(generateAuthId);
		assertTrue(authenticatedMember.isPresent());
	}
	
	@Test
	public void customFinder() {
		
		SportyMember member = new SportyMember();
		member.setEmail("test@amdatu.org");
		member.setMaxDeadlift(160);
		
		serviceToTest.save(member);
		
		List<SportyMember> members = serviceToTest.findStrongMembers();
		assertFalse(members.isEmpty());
	}
	
	@After
	public void after() {
		cleanUp(this);
	}
}
