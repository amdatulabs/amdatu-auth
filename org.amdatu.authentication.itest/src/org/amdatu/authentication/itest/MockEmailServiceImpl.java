/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.authentication.itest;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.amdatu.authentication.api.Member;
import org.amdatu.authentication.api.PasswordResetEmailService;
import org.apache.felix.dm.annotation.api.Component;

@Component
public class MockEmailServiceImpl implements PasswordResetEmailService, MockEmailService{

	private List<EmailEvent> m_eventLog = new CopyOnWriteArrayList<>();
	
	@Override
	public void sendPasswordResetEmail(Member member, String resetToken) {
		m_eventLog.add(new EmailEvent(member.getEmail(), resetToken));
	}
	
	@Override
	public List<EmailEvent> getEventLog() {
		return m_eventLog;
	}
	
	@Override
	public void reset() {
		m_eventLog.clear();
	}
	
	public static class EmailEvent {
		private final String email;
		private final String token;
		
		public EmailEvent(String email, String token) {
			this.email = email;
			this.token = token;
		}

		public String getEmail() {
			return email;
		}

		public String getToken() {
			return token;
		}
	}
}
