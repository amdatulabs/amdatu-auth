/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.authentication.itest;

import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.amdatu.testing.mongo.OSGiMongoTestConfigurator.configureMongoDb;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.amdatu.authentication.api.AuthService;
import org.amdatu.authentication.api.Member;
import org.amdatu.authentication.itest.MockEmailServiceImpl.EmailEvent;
import org.amdatu.testing.configurator.TestConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This tests verifies if a service is available for injection using just the basic AuthService interface.
 */
public class StandardAuthServiceTest {
	private volatile AuthService<Member> serviceToTest;
	private volatile MockEmailService m_emailService;

	@Before
	public void setup() {
		configure(this).add(createServiceDependency().setService(AuthService.class).setRequired(true))
				.add(configureMongoDb())
				.add(createServiceDependency().setService(MockEmailService.class).setRequired(true)).apply();
	}
	
	@Test
	public void checkInjected() {
		assertNotNull(serviceToTest);
	}
	
	@Test
	public void passwordResetEmail() {
		Member m = new Member();
		m.setEmail("test@amdatu.org");
		serviceToTest.save(m);
		
		serviceToTest.preparePasswordReset("test@amdatu.org");
		
		List<EmailEvent> eventLog = m_emailService.getEventLog();
		assertEquals(1, eventLog.size());
		assertEquals("test@amdatu.org", eventLog.get(0).getEmail());
		assertNotNull(eventLog.get(0).getToken());
	}
	
	@After
	public void cleanup() {
		TestConfigurator.cleanUp(this);
	}
}
