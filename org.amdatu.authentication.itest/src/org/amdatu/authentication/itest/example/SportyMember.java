/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.authentication.itest.example;

import org.amdatu.authentication.api.Member;

public class SportyMember extends Member {

	private double m_bodyweight;
	private double m_maxSquat;
	private double m_maxDeadlift;
	private double m_maxSnatch;
	private double m_maxCleanAndJerk;

	public double getBodyweight() {
		return m_bodyweight;
	}

	public void setBodyweight(double bodyweight) {
		m_bodyweight = bodyweight;
	}

	public double getMaxSquat() {
		return m_maxSquat;
	}

	public void setMaxSquat(double maxSquat) {
		m_maxSquat = maxSquat;
	}

	public double getMaxDeadlift() {
		return m_maxDeadlift;
	}

	public void setMaxDeadlift(double maxDeadlift) {
		m_maxDeadlift = maxDeadlift;
	}

	public double getMaxSnatch() {
		return m_maxSnatch;
	}

	public void setMaxSnatch(double maxSnatch) {
		m_maxSnatch = maxSnatch;
	}

	public double getMaxCleanAndJerk() {
		return m_maxCleanAndJerk;
	}

	public void setMaxCleanAndJerk(double maxCleanAndJerk) {
		m_maxCleanAndJerk = maxCleanAndJerk;
	}
}
