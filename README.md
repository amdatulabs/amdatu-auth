# README #

This project contains components to easily setup authentication, authorisation and user management in a project. The project is not primarily aimed at integration with other user management systems, but aimed at projects that want to include auth and user management as part of the project.

# What this project is and is not #

**It is** an easy to use, but very opinionated User management and Authentication backend. It offers a lot of functionality out of the box which may be useful for *some* projects. By being opinionated, **it is not** designed to be the ultimate flexible / plugable project to fit the needs of *all* projects. If you're looking to setup user management and authentication within your project, these components might be useful. 

# Project origins #

The project is based on code which is part of a closed source project. Since we see that similar projects re-implement the same functionality, we tried to extract the code and generalise it enough to make it reusable.

# Components #

The project consists of the following bundles:

* org.amdatu.authentication.api: A generic Member class and various service interfaces implemented in the other bundles. This API contains nothing web specific.
* org.amdatu.authentication.mongo: Implementation of the AuthService based on Mongo, which is also taking care of CRUD of Members. 
* org.amdatu.authentication.web: A web specific service that handles common functionality such as getting a user based on a HttpServletRequest.
* org.amdatu.authentication.rest: A REST resource that handles authentication and CRUD on Members.

The project comes with a [companion UI project](https://github.com/amdatu/amdatu-auth-angular) that contains a UI implementation based on TypeScript and AngularJS. If you're using Angular this is probably a good starting point. If you're using an alternative UI technology, it's easy to create a UI on top of the REST resource offered by this project. The UI project is hosted on GitHub so we can integrate with Bower. Install into your own UI project using Bower:

```
#!javascript
bower install amdatu-auth-angular
```


# Getting started #

### Creating your own Member type and service 

Almost every project requires it's own type of User/Member. This means having different properties on a Member that are specific for the project. We could decouple this model completely from authorisation, but this would make it harder to offer out of the box user management functionality. Instead this project has an extensible Member type so that you can easily add your own properties.

Create your own type that extends from *org.amdatu.authentication.api.Member* and add properties to this type. Next create your own service interface that extends from AuthService, with the generic type that you just created.

```
#!java
public interface CustomAuthService extends AuthService<SportyMember>{
	List<SportyMember> findStrongMembers();
}
```

Next, implement the service by extending from *BaseMongoAuthService*, again with your own type as type parameter, and implement from your own service interface. The base class gives you all the out of the box functionality, but this way you can easily add your own finder methods etc.

Although you have to do *some* work to get this up and running, the effort is pretty minimal, in return of a customisable Member type and service.

Note that there is also already a service implementation that just implements the AuthService based on the bare Member type. This service is used by the REST component, and you can optionally use it if you don't need any customisation.

```
#!java

@Component
public class CustomAuthServiceImpl extends BaseMongoAuthService<SportyMember> implements CustomAuthService {

	@ServiceDependency
	private volatile MongoDBService m_mongoDbService;
	
	@ServiceDependency
	private volatile PasswordResetEmailService m_emailService;
	
	public CustomAuthServiceImpl() {
		super("members", SportyMember.class);
	}
	
	@Start
	public void start() {
		super.start();
	}

	@Override
	protected MongoDBService getMongoDbService() {
		return m_mongoDbService;
	}

	@Override
	public List<SportyMember> findStrongMembers() {
		return mapCursorToList(m_collection.find().greaterThanEquals("maxDeadlift", 150.0));
	}

	@Override
	protected PasswordResetEmailService getEmailService() {
		return m_emailService;
	}
}
```

You can see examples of implementing and working with (custom) AuthServices in the integration tests of the project.

### Implementing an Email service ###

The project contains password reset functionality. Part of the reset process is sending an email to the user. Because this is probably something you will need to customise with your own email templates, from address etc., there is no default implementation available. An example implementation is as below. Of course we recommend using Amdatu Email to implement the actual sending of email.

```
#!java
@Component
public class MockEmailService implements PasswordResetEmailService{

	@Override
	public void sendPasswordResetEmail(Member member, String resetToken) {
		System.out.println("Sending mail to " + member.getEmail() + " with token " + resetToken);
	}

}
```